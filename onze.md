# 1. Leçon 1.

# 1.1. Partie intermédiaire.	

# 1.1.1. Nom de la leçon.

```
Introducing SAP Analytics Cloud Planning
```

# 1.1.1.1. Question 1.

Question:

```
In SAP Analytics Cloud planning you can use stories, SAP Analytics Cloud Add-in for Microsoft Office, and SAP Analysis for Office to perform planning.
```

Réponse:

```
True
```

Justification:

```
SAP Analytics Cloud Add-in for Microsoft Office, and SAP Analysis for Office to perform planning can all be used for planning.
```

## 2. Leçon 2.

## 2.1. Nom de la leçon.

```
Planning in Stories
```

## 2.1.1. Question 1.

Question:

```
The Planning Panel is a built-in feature used to distribute values among dimension members in a story. 
```

Justification:

```
The Planning Panel is a built-in feature in a story used to distribute values among members.
```

## 2.1.2. Question 2.

Question:

```
What dimension allows the creation of new members from a story?
```

Réponse:

```
Generic
```

Justification:

```
Generic and organization dimensions allow the creation of new members from a story. 
```

## 2.1.3. Question 3.

Question:

```
Cell locking is defined in the model.
```

Réponse:

```
False
```

Justification:

```
Cell locking is used in the story with no configuration needed in the model.
```

## 2.1.4. Question 4.

Question:

```
What are the two types of Versions for planning models?
```

Réponse:

```
Public and Private
```

Justification:

```
The two types of Versions for planning models are public and private.
```

### 3. Leçon 3.

### 3.1. Nom de la leçon.

```
Configuring Data Actions
```

### 3.1.1. Question 1.

Question:

```
The results of an advanced formula step are not stored in the SAC database.
```

Réponse:

```
False
```

Justification:

```
Advanced formula results are stored in the SAP Analytics Cloud database. 
```

### 3.1.2. Question 2.

Question:

```
Your users are complaining that their data actions are running slower than normal. You should follow up by running everyone's data actions to see for yourself. 
```

Réponse:

```
False
```

Justification:

```
You should access the built-in performance tracking content and run the Data Action Statistics and Analysisstory.
```

### 3.1.3. Question 3.

Question:

```
When you filter data with an input control, you can automatically run the data action for the same member. 
```

Réponse:

```
True
```

Justification:

```
You can link a data action parameter to input controls and story filters.
```

### 3.1.4. Question 4.

Question:

```
What are the types of action steps available in a Data Action?
```

Réponses:

```
Advanced Formulas step
```

```
Cross-Model Copy step
```

```
Copy step
```

Justification:

```
Six types of data actions are available: Copy, cross-model copy, embedded, allocations, currency, and advanced formulas.
```

### 3.1.5. Question 5.

Question:

```
You are limited to 5 steps in a data action planning sequence.
```

Réponse:

```
False
```

Justification:

```
You can have as many steps as you need.
```

### 3.1.6. Question 6.

Question:

```
What are the advantages of multi actions?
```

Réponses:

```
Scheduling
```

```

```

```
Lower cost of development
```

Justification:

```
Multi actions can be scheduled in the SAP Analytics Cloud calendar and multiple data actions can be bundled into one multi action. By including one multi action in a story instead of several data action, the cost of developing stories is lowered.
```

### 3.1.7. Question 7.

Question:

```
How can you make a copy step more flexible for the end-users?
```

Réponse:

```
Use a parameter to allow them to choose dimension members
```

Justification:

```
Parameters add flexibility for end-users.
```

### 3.1.8. Question 8.

Question:

```
What are the options for defining advanced data actions?
```

Réponses:

```
In the Visual Editor
```

```
In the Script Editor
```

Justification:

```
You can use the visual or script editors to create advanced data actions.
```

### 3.1.9. Question 9.

Question:

```
When using a cross-model copy step, what must you include?
```

Réponse:

```
A filter on the Version dimension to choose which version to copy from
```

Justification:

```
You must filter on the source version to copy data between models.
```

#### 4.1 Leçon 4.

#### 4.1. Nom de la leçon.

```
Allocating Planning Data
```

#### 4.1.1. Question 1.

Question:

```
Allocations are performed in order to identify cost by responsible unit.
```

Réponse:

```
True
```

Justification:

```
Allocations are performed in order to identify the total cost by responsible unit.
```

#### 4.1.2. Question 2.

Question:

```
The booking account is used to send a summarized value to target cost centers.
```

Réponse:

```
True
```

Justification:

```
The booking account is used to send a summarized value to target cost centers.
```

#### 4.1.3. Question 3.

Question:

```
An allocation can be run from the story ribbon or via a data action.
```

Réponse:

```
True
```

Justification:

```
An allocation can be run from the story ribbon and / or a data action. Data actions can run all steps of an allocation process or individual steps.
```

##### 5. Leçon 5.

##### 5.1. Nom de la leçon.

```
Translating Currencies for Planning
```

##### 5.1.1. Question 1.

Question:

```

```

Réponse:

```
True
```

Justification:

```
Currency data actions are run to record results into the database. Conversion measures are calculated on the fly.
```

##### 5.1.2. Question 2.

Question:

```
You are analyzing currency values in a story. You can access any conversion measure from the model and you can also create more conversion measures in the story.
```

Réponse:

```
True
```

Justification:

```
Model-based conversion measures are available in stories and you can create your own in stories if needed.
```

###### 6. Leçon 6.

###### 6.1. Nom de la leçon.

```
Forecasting and Simulating Planning Data
```

###### 6.1.1. Question 1.

Question:

```
A Predictive Forecast requires which objects in a story?
```

Réponses:

```
Data table
```

```
Planning model
```

Justification:

```
A Predictive Forecast requires a data table that uses a planning model as a data source.
```

###### 6.1.2. Question 2.

Question:

```
A Value Driver Tree can be created based on any model type.
```

Réponse:

```
True
```

Justification:

```
A Value Driver Tree can be created on planning as well as analytic models.
```

###### 6.1.3. Question 3.

Question:

```
What are the models in advanced options when running a predictive forecast?
```

Réponses:

```
Automatic forecast
```

```
Linear regression
```

Justification:

```
Linear regression and automatic forecast are included in the advanced options.
```

###### 6.1.4. Question 4.

Question:

```
With a rolling forecast, the time interval is static.
```

Réponse:

```
False
```

Justification:

```
Time determination can be dynamic.
```

Leçon 7.

###### 6.1.1.1. Nom de la leçon.

```
Coordinating and Controlling Processes
```

###### 6.1.1.1. Question 1.

Question:

```
What is the purpose of a validation rule?
```

Réponse:

```
To prevent improper data entry and planning operations.
```

Justification:

```
Validation rules check for invalid member combinations. 
```

###### 6.1.1.2. Question 2.

Question:

```
The communications tools used in collaboration include:
```

Réponses:

```
Comments
```

```
Calendar
```

Justification:

```

```

###### 6.1.1.3. Question 3.

Question:

```
In a planning process, a Reviewer is required.
```

Réponse:

```
False
```

Justification:

```
Reviewers are optional. They are only needed when approvals are required.
```

###### 6.1.1.4. Question 4.

Question:

```
Data audit reports include the time stamp, user, previous value, new value, and delta value for data changes.
```

Réponse:

```
True
```

Justification:

```
Data audit records the time stamp, user, previous value, new value, and delta value whenever there is a data change.
```

###### 6.1.1.5. Question 5.

Question:

```
What is one criteria for defining data locks?
```

Réponse:

```
Data locking must be enabled in the model.
```

Justification:

```
Data locks are configured in the model.
```

###### 6.1.1.6. Question 6.

Question:

```
Where is a Process created?
```

Réponse:

```
Calendar
```

Justification:
