# 1. Leçon 1.

# 1.1 Nom de la leçon.

```
Getting Started with SAP Analytics Cloud
```

# 1.1.1 Questions et leçons niveau beginner.

# 1.1.1.1 Question une.

Question:

```
The Data Access Language option determines the preferred language for which areas of SAP Analytics Cloud?
```

Réponses:

```
Live Data 
```

```
Text
```

Justiification:

```
The Data Access Language option determines the preferred language for Live Data and Text but not imported data or help.
```

# 1.1.1.2 Question deux.

Question:

```
Your users need a story where the pages adjust the size and location of the elements on the page depending on the device, or size of device, being used to view the story. What page type should you use?
```

Réponse:

```
Responsive
```

Justification:

```
A responsive page adjusts the size and location of the elements on the page depending on the device, or size of device, being used to view the story.
```

# 1.1.1.3 Question trois.

Question:

```
What are the three primary access levels you can assign when sharing files?

```

Réponses:

```
View
```

```
Edit
```

```
Full Control
```

Justification:

```
The access levels available when sharing are View, Edit, Full Control, and Custom.
```

# 1.1.1.4 Question quatre.

Question:

```
SAP Analytics Cloud includes Enterprise Planning, Augmented Analytics, Enterprise Platform Services, and Business Intelligence. True or false?
```

Réponse:

```
True
```

Justification:

```
SAP Analytics Cloud includes Enterprise Planning, Augmented Analytics, Enterprise Platform Services, and Business Intelligence.
```

# 1.1.1.5 Question cinq.

Question:

```
 You want to use the embedded help function in SAP Analytics Cloud to support you. What is offered by the embedded help? 
```

Réponses:

```
Guided help tutorials
```

```
Videos
```

Justification:

```
The SAP Analytics cloud embedded help function includes guided help tutorials, videos, and classical help documents, but no live chat or chatbot.
```

# 1.1.1.6 Question six.

Question:

```
When you are scheduling, to how many non-SAP Analytics Cloud recipients can you distribute?
```

Réponse:

```
3
```

Justification:

```
You can distribute to only 3 non-SAP Analytics Cloud recipients.
```

# 1.1.1.7 Question sept.

Question:

```
the SAP Analytics Cloud Catalog, what access do you have to the files?
```

Réponse:

```
View
```

Justification:

```

```

# 1.1.1.8 Question huit.

Question:

```
When scheduling a publication, what are the two file type options?
```

Réponses:

```
.pptx
```

```
.pdf
```

Justification:

```
When scheduling a publication, the only file types available are .pdf and .pptx
```

## 2. Leçon 2.

## 2.1. Nom de la leçon

```
Establishing Data Sources and Connections
```

## 2.1.1. Question une.

Question:

```
You use Data Analyzer for a quick analysis of data, and you save your analysis results. What is the file type? 
```

Réponse:

```
Insight
```

Justification:

```
Data Analyzer saves an Insight.
```

## 2.1.2 Question deux.

Question:

```

```

Réponses:

```
SAP Datasphere
```

```
SAP HANA Live Views
```

```
SAP Analytics Cloud Models
```

```
SAP BW Queries
```

Justification:

```
The data sources for Data Analyzer are SAP BW queries, SAP HANA Live views, SAP Datasphere and SAP Analytics Cloud models.
```

## 2.1.3 Question trois.

Question:

```
In SAP Analytics Cloud you use an import connection to an SAP ERP system. Your colleague says that no data is stored in SAP Analytics Cloud. Is the statement of your colleague true or false?
```

Réponse:

```
False
```

Justification:

```
With an import connection, data is duplicated from the on-premise datasource and stored in SAP Analytics Cloud.
```

## 2.1.4 Question quatre.

Question:

```
With a live data connection, where is the data security defined?
```

Réponse:

```
Data security is defined in the source system.
```

Justification:

```
With a live connection data security is defined in the source system.
```

### 3. Leçon 3.

### 3.1. Nom de la leçon

```
Using Modeling
```

### 3.1.1. Question une.

Question:

```
What elements can you import data into in SAP Analytics Cloud? 
```

Réponses:

```
Dimensions
```

```
Analytic Models
```

```
Planning Models
```

Justification:

```
Data can be imported in dimensions, planning models, and analytic models.
```

### 3.1.2. Question deux.

Question:

```
Which data source does not require a model be created for it in SAP Analytics Cloud for use in stories? 
```

Réponse:

```
SAP Datasphere
```

Justification:

```
SAP Datasphere requires a connection be created in SAP Analytics Cloud, but no SAP Analytics Cloud model is necessary
```

### 3.1.3. Question trois.

Question:

```

With an import model, when changes happen in the original data source, what happens in the SAP Analytic Model? 
```

Réponse:

```
Nothing. The changes in the original data source are not reflected in the model.
```

Justification:

```
Changes in the original data source are not reflected in the model.
```

### 3.1.4. Question quatre.

Question:

```
It is possible to concatenate a First Namedimension with a Last Name dimension into one Full Name dimension in a live model.
```

Réponse:

```
False
```

Justification:

```
You cannot manipulate any data in a live model.
```

### 3.1.5. Question cinq.

Question:

```
What elements make up the framework for viewing data in SAP Analytics Cloud?
```

Réponses:

```
Dimensions
```

```
Measures
```

Justification:

```
Dimensions and measures are the framework for viewing data.
```

### 3.1.6. Question six.

Question:

```
A model where the data is read-only must contain a date dimension.
```

Réponse:

```
False
```

Justification:

```
A date dimension is optional in a model that is read-only.
```

### 3.1.7. Question sept.

Question:

```
What are the types of datasets in SAP Analytics Cloud?
```

Réponses:

```
Embedded
```

```
Public
```

Justification:

```
SAP Analytics Cloud has two types of datasets: Embedded and Public.
```

### 3.1.8. Question huit.

Question:

```
What are the styles of models?
```

Réponses:

```
Analytic
```

```
Planning
```

Justification:

```
In SAP Analytics Cloud there are two styles of models: Analytic and Planning.
```

### 3.1.9. Question neuf.

Question:

```
A dataset can have column-based security applied to it.
```

Réponse:

```
False
```

Justification:

```

```

#### 4. Leçon 4.

#### 4.1. Nom de la leçon.

```
Using Basic and Advanced Stories
```

#### 4.1.1. Question une.

Question:

```
You want to create a story with charts and need a data source to populate your charts with data. Which option can be used as a data source?
```

Réponses:

```
Model
```

```
Dataset
```

Justification:

```
The two possible data sources for a story are a Model and a Dataset.
```

#### 4.1.2. Question deux.

Question:

```
You have used the SAP Analytics Cloud Add-In for Microsoft Excel for a business intelligence data analysis. Your colleague says that you can also use the SAP Analytics Cloud Add-In for Microsoft Excel for planning scenarios. Is the statement of your colleague true or false?
```

Réponse:

```
True
```

Justification:

```
The SAP Analytics Cloud Add-In for Microsoft Office is available for business intelligence data analysis and for planning activities.
```

#### 4.1.3. Question trois.

Question:

```
SAP encourages you to use the Classic Design Experience.
```

Réponse:

```
False
```

Justification:

```
You should use the Optimized Design Experience.
```

#### 4.1.4. Question quatre.

Question:

```
You created a story and want to share it with an external agent, who does not have SAP Analytics Cloud. You decide to export the story as a file. Which file type can be used?
```

#### Réponse:

```
pdf
```

Justification:

```
When sharing a story externally, the file type is pdf, pptx, or Google Slides.
```

#### 4.1.5. Question cinq.

Question:

```
What scripting language is used in the advanced story interface?
```

Réponse:

```
JavaScript
```

Justification:

```
JavaScript is the scripting language used in the Analytic Applications design interface.
```

#### 4.1.6. Question six.

Question:

```
Your users want a static view of data with no user interactions at all. You can use scripting to accomplish this.
```

Réponse:

```
True
```

Justification:

```
An analytic application can be extremely static.
```

#### 4.1.7. Question sept.

Question:

```
You need to export some data from your story into a .csv file. What do you do?
```

Réponse:

```
Choose a widget that displays data and use its More Actions menu.
```

#### 4.1.8. Question huit.

Question:

```

You have created a story bookmark. Your colleague asked you to share this bookmark. Can you?
```

Réponse:

```
Yes
```

Justification:

```
Bookmarks can be shared.
```

#### 4.1.9. Question neuf.

Question:

```
What are the working modes of a story?
```

Réponse:

```
View
```

```
Edit
```

Justification:

```
All SAP Analytics Cloud stories have View and Edit working modes.
```

#### 4.1.10. Question dix.

Question:

```
For a story element, what is similar to a context menu?
```

Réponse:

```
The More Actions button is similar to a content menu.
```

Justification:

```
The More Actions button is similar to a content menu.
```

##### 5. Leçon 5.

##### 5.1. Nom de la leçon

```
Using Planning
```

##### 5.1.1. Question une.

Question:

```
What can help prevent data input for invalid member combinations across dimensions? 
```

Réponse:

```
Validation Rules
```

Justification:

```
Validation Rules help prevent data input for invalid member combinations across dimensions.
```

##### 5.1.2. Question deux.

Question:

```
SAP Analytics Cloud planning functionality can be integrated with any other SAP planning systems. True or false?
```

Réponse:

```
True
```

Justification:

```
SAP Analytics Cloud planning functionality can be integrated with other SAP on-premise planning systems.
```

##### 5.1.3. Question trois.

Question:

```
A private version cannot be changed after saving a story. True or false?
```

Réponse:

```
False
```

Justification:

```
A private version can be changed until it is published, but it can be saved until publishing.
```

##### 5.1.4. Question quatre.

Question:

```
You have created a story using a planning model and notice that a Value Driver Tree can be defined. Why do you use a Value Driver Tree?
```

Réponse:

```
To view a planning simulation graphically
```

Justification:

```
Two possible uses of a Value Driver Tree are to view a planning simulation graphically and to leverage driver-based planning.
```

###### 6. Leçon six.

###### 6.1. Nom de la leçon.

```
Using Collaboration Features
```

###### 6.1.1. Question une.

Question:

```
From where can you access Search to Insight?
```

Réponse:

```
The SAP Analytics Cloud Home screen
```

Justification:

```
You can access Search to Insight from the The SAP Analytics Cloud Home screen.
```

###### 6.1.2. Question deux.

Question:

```
What lets you create 1 or more models that learn from your historical data?
```

Réponse:

```
Smart Predict
```

Justification:

```
Only Smart Predict creates 1 or more models that learn from your historical data.
```

###### Leçon 7.

Using Basic Augmented Analytics in SAP Analytics Cloud

###### 6.1.3. Question trois.

Question:

```
How many characters are comments limited to in SAP Analytics Cloud? 
```

Réponse:

```
1,000
```

Justification:

```
Comments in SAP Analytics Cloud are limited to 1,000 characters.
```

###### 6.1.4. Question quatre.

Question:

```
What features does the SAP Analytics Cloud calendar provide?
```

Réponses:

```
Collaborate
```

```
Assign Reviewer
```

Justification:

```
The SAP Analytics Cloud calendar allows you to assign reviewers and collaborate. You can also set and track status, add reminders, and view due dates.
```

###### 6.1.5. Question cinq.

Question:

```
The SAP Analytics Cloud Calendar can be used to organize work flows. True or false?
```

Réponse:

```
True
```

Justification:

```

```

###### 6.1.6. Question six.

Question:

```
If you schedule a publication, what management options do you have for the schedule?
```

Réponse:

```
Discontinue
```

```
Modify
```

Justification:

```
From the calendar you can modify and discontinue a schedule. You can also view, copy, or delete a schedule.
```
