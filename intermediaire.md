# 1.Leçon 1.

# 1.1. Partie intermédiaire.

# 1.1.1. Nom de la leçon.

```
Getting Started with Stories
```

# 1.1.1.1 Question une.

Question:

```
You use a flat file as a data source for your story. What is a possible option? 
```

Réponse:

```
Use the Data Preparation area to manipulate the data
```

Justification:

```
You can use the Data Preparation area to manipulate the data.
```

# 1.1.1.2. Question deux.

Question:

```
What is a benefit of Optimized Design Experience?
```

Réponse:

```
It has improved Tooltip Interactions
```

Justification:

```
One benefit of the Optimized Design Experience is improved Tooltip Interactions.
```

# 1.1.1.3. Question trois.

Question:

```
What is a data source for a story in SAP Analytics Cloud? 
```

Réponses:

```
Outside data source
```


```
File
```


```
SAP Analytics Cloud Model
```

Justification:

```
Data for a story can come from a file, a SAP Analytics Cloud model or dataset, or an outside data source.
```

# 1.1.1.4. Question quatre.

Question:

```
You can use data from a non-SAP system in your story.
```

Réponse:

```
True
```

Justification:

```
Data for SAP Analytics Cloud stories can come from both SAP and non-SAP data sources.
```

# 1.1.1.5. Question cinq.

Question:

```
SAP Analytics Cloud stories are browser-based.
```

Réponse:

```
True
```

Justification:

```
SAP Analytics Cloud stories are browser-based.
```

# 1.1.1.6. Question six.

Question:

```
Which story element does not exist in the Optimized Design Experience?
```

Réponse:

```
Grid page
```

Justification:

```
The Grid page does not exist in the Optimized Design Experience.
```

## 2. Leçon 2.

## 2.1. Nom de la leçon.

```
Building Stories
```

## 2.1.1. Question une.

Question:

```
What is a feature of a story template?
```

Réponses:

```
Templates contain widgets that are empty placeholders
```


```
Templates reduce the cost of development
```

Justification:

```
Templates contain widgets that are empty placeholders and reduce the cost of development.
```

## 2.1.2. Question deux.

Question:

```
You need to transform data for your story. What is a transformation option for manipulating data? 
```

Réponses:

```
Custom Expression Editor
```


```
Transform Bar
```

Justification:

```
The two transformation options are the Transform Bar and the Custom Expression Editor.
```

## 2.1.3. Question trois.

Question:

```
SAP encourages you to use Classic Design Experience when creating stories.
```

Réponse:

```
False
```

Justification:

```
SAP encourages you to use Optimized Design Experience as much as possible.
```

### 3. Leçon 3.

### 3.1. Nom de la leçon

```
Configuring Story Elements
```

### 3.1.1. Question une.

Question:

```
To improve story performance, you should use JPG images.
```

Réponse:

```
False
```

Justification:

```
SVG images are the optimal choice, then PNG, and finally, JPG.
```

### 3.1.2. Question deux.

Question:

```
When using a model in a story, all table features are available, regardless of the model type.
```

Réponse:

```
False
```

Justification:

```
When using a model in a story, the available table features such as calculations, data entry, and automatic time determination depend on the type of model.
```

### 3.1.3. Question trois.

Question:

```
You want to create a story in SAP Analytics Cloud and present the data of storage locations in the Geo Map widget. What model you can use for the map?
```

Réponses:

```
Import models manually uploaded
```


```

```

Justification:

```
Import models using a manual upload and live models using SAP HANA as a data source are the two geo models.
```

### 3.1.4. Question quatre.

Question:

```
What can story preferences be used for?
```

Réponses:

```
Format new responsive pages
```


```
Format existing responsive pages
```

Justification:

```
Story preferences can be used to format existing and new responsive pages. The number of decimals for charts and tables are in their styles.
```

### 3.1.5. Question cinq.

Question:

```
What programming language enables you to create custom charts and graphs?
```

Réponse:

```
R
```

Justification:

```
R is the programming language that enables you to create custom charts and graphs.
```

### 3.1.6. Question six.

Question:

```
You can view data point comments only in the Comment widget.
```

Réponse:

```
False
```

Justification:

```
You can view data point comments in a table cell, a comment column, or in the Comment widget.
```

### 3.1.7. Question sept.

Question:

```
You cannot enter a data point comment on a single cell in a table, but you can make a comment on the entire table. What is the problem?
```

Réponse:

```
The Allow Data Point Commentssetting for the table is not enabled
```

Justification:

```
In order to add data point comments, you first must enable the Allow Data Point Comments option in the Properties section of the builder panel for the table.
```

### 3.1.8. Question huit.

Question:

```
How do you alter the colors on charts based on thresholds?
```

Réponse:

```
Conditional formatting
```

Justification:

```

```

### 3.1.9. Question neuf.

Question:

```
You can apply a style only to the table header.
```

Réponse:

```
False
```

Justification:

```
You can apply a style to any table region.
```

### 3.1.10. Question dix.

Question:

```
Thresholds can be defined at both the model and story levels.
```

Réponse:

```
True
```

Justification:

```
Thresholds can be defined at both the model and story levels.
```

#### 4. Leçon.

#### 4.1. Nom de la leçon.

```
Manipulating Data in Stories
```

#### 4.1.1. Question une.

Question:

```
In your story you use one chart and one table, which are based on the same data source with a prompt. Your colleague says that the response on the prompt will automatically filter the data of the chart and the table. Is the statement of your colleague true or false?
```

Réponse:

```
True
```

Justification:

```
When a data source for a story includes variables/prompts, all tables and charts based on that data source are filtered to the variable response.
```

#### 4.1.2. Question deux.

Question:

```
You can use IF statements in SAP Analytics Cloud story calculations.
```

Réponse:

```
True
```

Justification:

```
IF statements can be used in story calculations.
```

#### 4.1.3. Question trois.

Question:

```
You have created a story with two charts. The second chart should be filtered automatically by the filter used for the first chart. What do you use to enable this scenario?
```

Réponse:

```
Linked Analysis
```

Justification:

```
Linked Analysis allows you to enable one component to filter other components in a story.
```

#### 4.1.4. Question quatre.

Question:

```
A story can contain only one data source.
```

Réponse:

```
False
```

Justification:

```
You can create a story with visualizations using data from multiple models and datasets.
```

#### 4.1.5. Question cinq.

Question:

```
You are asked to create a story with a table where the users can choose which dimension is displayed in the rows of the table. How could you meet this request? 
```

Réponse:

```
Use a dimension input control
```

Justification:

```
You can use input controls to allow users to change what specific dimensions and/or measure are displayed in a table or chart.
```

#### 4.1.6. Question six.

Question:

```
Blending enables you to join a primary data source with secondary data sources that contain common linked dimensions.
```

Réponse:

```
True
```

Justification:

```

```

#### 4.1.7. Question sept.

Question:

```
You can create calculations only on measures.
```

Réponse:

```
False
```

Justification:

```
You can create calculated measures and dimensions.
```

#### 4.1.8. Question huit.

Question:

```
When using an input control for time, the time ranges must be fixed.
```

Réponse:

```
False
```

Justification:

```
When using an input control for time, the time ranges can be either fixed or dynamic.
```

#### 4.1.9. Question neuf.

Question:

```
What are supported calculation types?
```

Réponses:

```
Calculated Measures
```

```
Aggregation
```

```
Restricted Measures
```

Justification:

```
Calculated measures, restricted measures, and aggregation are supported.
```

#### 4.1.10. Question dix.

Question:

```
In what order can data be sorted?
```

Réponses:

```

```

```
Custom
```

```
Ascending
```

Justification:

```
Data can be sorted in ascending, descending, or with a custom sort order.
```

##### 5. Leçon.

##### 5.1. Nom de la leçon.

```
Presenting Stories
```

##### 5.1.1. Question un.

```
SAP Digital Boardroom is a fully automated tool that instantly creates relevant visualizations during a meeting. No preparation is needed before the meeting.
```

Réponse:

```
False
```

Justification:

```
The content that needs to be displayed must be created and prepared in advance.
```

##### 5.1.2. Question deux.

Question:

```
You have created a story and want to show it on a mobile device. Which mobile device is supported by SAP Analytics Cloud? 
```

Réponses:

```

Android
```


```
iOS
```

Justification:

```
SAP Analytics Cloud supports the iOS and Android mobile devices.
```

##### 5.1.3. Question trois.

Question:

```
Only canvas pages can be viewed on a mobile device using the SAP Analytics Cloud mobile app.
```

Réponse:

```
False
```

Justification:

```
Responsive pages can be viewed in the app, but not canvas pages.
```

##### 5.1.4. Question quatre.

Question:

```
You can view a digital boardroom on multiple screens.
```

```
True
```

Justification:

```

```
