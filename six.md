# 1. Leçon 1.

# 1.1. Partie intermédiaire.

# 1.1.1. Nom de la leçon.

```
Explaining the Basics of Data Structures in SAP Analytics Cloud
```

# 1.1.1.1. Question 1.

Question:

```
What supports scheduled data uploads?
```

Réponse:

```
Import model
```

Justification:

```
Only import models support scheduled data uploads.
```

# 1.1.1.2. Question 2.

Question:

```
A planning model requires which dimensions?
```

Réponses:

```
Version
```

```
Date
```

Justification:

```
A planning model requires Version and Date dimensions.
```

# 1.1.1.3. Question 3.

Question:

```
Models always contain data.
```

Réponse:

```
False
```

Justification:

```
Live models do not contain data.
```

# 1.1.1.4. Question 4.

Question:

```
The account-based model only has one measure.
```

Réponse:

```
True
```

Justification:

```
A typical account-based model only has one measure.
```

# 1.1.1.5. Question 5.

Question:

```
In a live model, where is data access controlled?
```

Réponse:

```
The source system
```

Justification:

```
In a live model, data access is controlled in the source system for the model.
```

# 1.1.1.6. Question 6.

Question:

```
The account-based model does not support: 
```

Réponses:

```
Converted measures
```

```
Calculated measures
```

Justification:

```
You cannot create calculated measures or converted measures in an account-based model.
```

# 1.1.1.7. Question 7.

Question:

```

```

Réponse:

```
Unlimited 
```

Justification:

```
You can have as many measures as you need in a measure-based model.
```

# 1.1.1.8. Question 8.

Question:

```
From where can stories access data?
```

Réponses:

```
Datasets
```

```
Import models
```

Justification:

```
Stories access data from datasets, import models, and live data models.
```

# 1.1.1.9. Question 9.

Question:

```
Embedded datasets are user-specific.
```

Réponse:

```
False
```

Justification:

```
Embedded datasets are story specific.
```

## 2. Leçon 2.

## 2.1. Nom de la leçon.

```
Designing and Creating Dimensions
```

## 2.1.1. Question 1.

Question:

```
Hierarchies are available in every dimension type.
```

Réponse:

```
False
```

Justification:

```
Hierarchies are not available for the Version dimension type.
```

## 2.1.2. Question 2.

Question:

```
You can create public date dimensions.
```

Réponse:

```
False
```

Justification:

```
The system creates date dimensions. They are model-specific.
```

## 2.1.3. Question 3.

Question:

```
Which is a valid dimension type? 
```

Réponses:

```
Public
```

```
Private
```

Justification:

```
The dimension valid types are Public and Private.
```

## 2.1.4. Question 4.

Question:

```
A model can contain multiples of which dimension type?
```

Réponses:

```
Generic
```

```
Date
```

Justification:

```
A model can contain multiple Date and Generic dimension types.
```

## 2.1.5. Question 5.

Question:

```
Which dimension cannot be shared among models?
```

Réponse:

```
Private
```

Justification:

```
A Private dimension cannot be shared among models because it exists only in the model where it was created.
```

## 2.1.6. Question 6.

Question:

```
What dimension types can you add when you create a model in SAP Analytics Cloud?
```

Réponses:

```

```

```
Account
```

```
Generic
```

Justification:

```
When creating a model, you can add Account, Generic, and Organization dimension types. Version and date dimensions are added by the system. 
```

## 2.1.7. Question 7.

Question:

```
What is a type of hierarchy?
```

Réponses:

```
Level-based
```

```
Parent-Child
```

Justification:

```
The types of hierarchies are level-based and parent-child.
```

## 2.1.8. Question 8.

Question:

```
A Person Responsible property can be created in a Generic dimension.
```

Réponse:

```
True
```

Justification:

```
A Person Responsible property can be enabled in a Generic dimension.
```

## 2.1.9. Question 9.

Question:

```
Measure values are stored in models.
```

Réponse:

```
True
```

Justification:

```
Measure values are stored in models.
```

## 2.1.10. Question 10.

Question:

```
You can add calculated columns when importing into a dimension.
```

Réponse:

```
True
```

Justification:

```
You can add calculated columns when importing into a dimension.
```

### 3. Leçon 3.

### 3.1. Nom de la leçon.

```
Creating Import Models
```

### 3.1.1. Question 1.

Question:

```
What is a prerequisite for creating a custom time hierarchy?
```

Réponse:

```
Add time-related properties to the Date dimension
```

Justification:

```
Because time hierarchies are parent-child, you must add time-related properties to the Date dimension.
```

### 3.1.2. Question 2.

Question:

```
Dimension member formulas are only available for the account type dimension.
```

Réponse:

```
True
```

Justification:

```
Dimension member formulas are only available for the account type dimension.
```

### 3.1.3. Question 3.

Question:

```
What time pattern is supported for the Date dimension?
```

Réponse:

```
13x4
```

Justification:

```
The supported weekly time patterns are: 4-4-5, 4-5-4, 5-4-4, 13x4.
```

### 3.1.4. Question 4.

Question:

```
Where does SAP Analytics Cloud store exchange rates?
```

Réponse:

```
In a currency table
```

Justification:

```
Exchange rates are stored in a separate currency table.
```

### 3.1.5. Question 5.

Question:

```
How is the model's time unit granularity defined? 
```

Réponse:

```
By the Date dimension
```

Justification:

```
The model's time unit granularity is defined by the Date dimension.
```

### 3.1.6. Question 6.

Question:

```
When importing data into a model, which dimension has no import option?
```

Réponses:

```
Version
```

```
Date
```

Justification:

```
The Version and Date dimensions are system-generated so have no import option.
```

### 3.1.7. Question 7.

Question:

```
If just one step in a scheduled import fails, all steps fail.
```

Réponse:

```
False
```

Justification:

```
If just one step in a scheduled import fails, the subsequent steps can run if the import allows it.
```

### 3.1.8. Question 8.

Question:

```
How can the currency table be updated?
```

Réponses:

```
Flat File
```

```
BPC rate model
```

```
BW query
```

Justification:

```
The currency table can be updated via flat file, BPC rate model, or BW query.
```

### 3.1.9. Question 9.

Question:

```
What happens when you turn off the Planning option in Model Preferences? 
```

Réponse:

```
The Version dimension goes away
```

Justification:

```
The Version dimension goes away when the Planning option is turned off, thus creating an Analytic model where Version is not required.
```

#### 4. Leçon 4.

#### 4.1. Nom de la leçon.

```
Creating Live Models
```

#### 4.1.1. Question 1.

Question:

```
Data Analyzer can use import connections.
```

Réponse:

```
False
```

Justification:

```
The data analyzer can use live connections.
```

#### 4.1.2. Leçon 2.

Question:

```
When accessing data from SAP S/4HANA, what is a possible data source?
```

Réponses:

```
Query
```

```
Core Data Services View
```

Justification:

```
Data sources for SAP S/4HANA are CDS Views and Queries.
```

#### 4.1.3. Question 3.

Question:

```
When using a WebI document as a data source, Web Intelligence variables are not accessible.
```

Réponse:

```
False
```

Justification:

```
Your SAP Analytics Cloud live model has access to variables defined in the Web Intelligence document.
```

#### 4.1.4. Question 4.

Question:

```
What is the source when acquiring data from SAP HANA Cloud into SAP Analytics Cloud?
```

Réponse:

```
Calculation View
```

Justification:

```
Calculation Views in SAP HANA Cloud are used to provide data to SAP Analytics Cloud.
```

#### 4.1.5. Question 5.

Question:

```
What SAP BW element is supported in a SAP Analytics Cloud model?
```

Réponses:

```
Variable
```

```
Structure
```

Justification:

```
Structures and Variables are supported BW elements, along with Time-dependent hierarchies, Variants and Personalization, and parallel processing of queries.
```

##### 5. Leçon 5.

##### 5.1. Nom de la leçon.

```
Working with Geographic Data
```

##### 5.1.1. Question 1.

Question:

```
Where can you get Point of Interest data?
```

Réponses:

```
SAP HANA model
```

```
.xlsx or .csv file
```

```
Esri shapefile
```

Justification:

```
You can get Point of Interest data from .xlsx or .csv files, SAP HANA models, and Esri shapefiles.
```

##### 5.1.2. Question 2.

Question:

```
When you geo-enrich data, the system concatenates the latitude and longitude columns.
```

Réponse:

```
True
```

Justification:

```
When you geo-enrich data, the system concatenates the latitude and longitude columns.
```

##### 5.1.3. Question 3.

Question:

```
Which dimension is automatically created when you geo-enrich the data?
```

Réponse:

```
Location
```

Justification:

```
The Location dimension is created when you geo-enrich the data.
```

###### 6. Leçon 6.

###### 6.1. Nom de la leçon.

```
Defining Data Security
```

###### 6.1.1. Question 1.

Question:

```
How is access to objects controlled?
```

Réponse:

```
via Roles
```

Justification:

```
Access to objects is controlled via Roles.
```

###### 6.1.2. Question 2.

Question:

```
With data access control, what permission can you set for dimension members?
```

Réponses:

```
Read
```

```
Write
```

Justification:

```
Dimension members can have Read or Write access set for them with data access control.
```
